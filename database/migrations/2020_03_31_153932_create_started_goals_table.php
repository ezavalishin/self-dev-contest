<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStartedGoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('started_goals', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->on('users')->references('id')->onDelete('cascade');

            $table->unsignedBigInteger('goal_id');
            $table->foreign('goal_id')->on('goals')->references('id')->onDelete('cascade');

            $table->unsignedInteger('score')->default(0);
            $table->unsignedInteger('photos_count')->default(0);
            $table->unsignedInteger('likes_count')->default(0);

            $table->boolean('is_active')->default(true);

            $table->string('comment')->nullable();

            $table->index(['is_active', 'score']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('started_goals');
    }
}
