<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCollageStoragePathToStartedGoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('started_goals', function (Blueprint $table) {
            $table->string('collage_storage_path')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('started_goals', function (Blueprint $table) {
            $table->dropColumn('collage_storage_path');
        });
    }
}
