<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoalPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goal_photos', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('started_goal_id');
            $table->foreign('started_goal_id')->on('started_goals')->references('id')->onDelete('cascade');

            $table->string('storage_path');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goal_photos');
    }
}
