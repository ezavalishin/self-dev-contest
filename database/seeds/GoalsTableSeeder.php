<?php

use Illuminate\Database\Seeder;

class GoalsTableSeeder extends Seeder
{
    private function data(): array
    {
        return [
            [
                'emoji' => '⚽️',
                'title' => 'Спорт',
                'slug' => 'sport'
            ],
            [
                'emoji' => '💭',
                'title' => 'Языки',
                'slug' => 'lang'
            ],
            [
                'emoji' => '😍',
                'title' => 'Хобби',
                'slug' => 'hobby'
            ],
            [
                'emoji' => '⏰',
                'title' => 'Сон',
                'slug' => 'sleep'
            ],
            [
                'emoji' => '🍏',
                'title' => 'Питание',
                'slug' => 'food'
            ],
            [
                'emoji' => '📙',
                'title' => 'Книги',
                'slug' => 'book'
            ],
            [
                'emoji' => '🥛',
                'title' => 'Вода',
                'slug' => 'water'
            ],
            [
                'emoji' => '🎥',
                'title' => 'Блог',
                'slug' => 'blog'
            ],
            [
                'emoji' => '😌',
                'title' => 'Медитация',
                'slug' => 'relax'
            ],
            [
                'emoji' => '🤘',
                'title' => 'Что-то своё',
                'slug' => 'rock'
            ],
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data() as $item) {
            \App\Goal::create($item);
        }
    }
}
