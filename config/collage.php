<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | This value determines the driver to be used for image handling.
    | Possible options:
    | "gd" : For Default GD Library.
    | "imagick" : For Imagick Library
    |
    */

    'driver' => 'gd',

    /*
    |--------------------------------------------------------------------------
    | Array of custom Generators
    |--------------------------------------------------------------------------
    |
    | This value determines if you are using any custom generators or override
    | any existing generators.
    |
    | This array should contain key value pairs. Where the key denoting
    | the number of images it will accept. And the value denoting the
    | class path of the Generator.
    |
    */
    'generators' => [
        2 => \App\Services\Collage\Generators\TwoImage::class,
        3 => \App\Services\Collage\Generators\ThreeImage::class,
        4 => \App\Services\Collage\Generators\FourImage::class,
        7 => \App\Services\Collage\Generators\SevenImage::class,
        14 => \App\Services\Collage\Generators\FourteenImage::class,
    ],
];
