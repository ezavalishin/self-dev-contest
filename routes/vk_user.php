<?php

/**
 * @var \Laravel\Lumen\Routing\Router $router
 */


$router->post('auth', 'MeController@auth');
$router->post('me/started-goal', 'MeController@startedGoal');

$router->get('goals', 'GoalController@index');

$router->post('started-goals', 'StartedGoalController@create');
$router->get('started-goals', 'StartedGoalController@index');
$router->get('started-goals/{id:[0-9]+}', 'StartedGoalController@show');
$router->post('started-goals/{id:[0-9]+}/upload-photo', 'StartedGoalController@uploadPhoto');
$router->post('started-goals/{id:[0-9]+}/like', 'StartedGoalController@like');
$router->post('started-goals/{id:[0-9]+}/generate-story', 'StartedGoalController@generateStory');
