<?php

namespace App\Http\Controllers\VkUser;

use App\Http\Controllers\Controller;
use App\Http\Resources\StartedGoalResource;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;

class MeController extends Controller
{
    public function auth()
    {
        $user = Auth::user();
        $startedGoalForUser = $user->getActiveStartedGoal();

        if ($startedGoalForUser) {
            $startedGoalForUserId = $startedGoalForUserId = $startedGoalForUser->id;
        } else {
            $startedGoalForUserId = false;
        }

        $user->setAttribute('started_goal_id', $startedGoalForUserId);

        return new UserResource($user);
    }

    public function startedGoal()
    {
        $user = Auth::user();
        $startedGoal = $user->startedGoals()->where('is_active', true)->first();

        if (!$startedGoal) {
            return [
                'data' => null
            ];
        }

        $startedGoal->load('lastGoalPhoto');

        return new StartedGoalResource($startedGoal);
    }
}
