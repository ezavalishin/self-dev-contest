<?php

namespace App\Http\Controllers\VkUser;

use App\Exceptions\ApiException;
use App\GoalPhoto;
use App\Http\Controllers\Controller;
use App\Http\Resources\GoalPhotoResource;
use App\Http\Resources\LikeResource;
use App\Http\Resources\StartedGoalResource;
use App\StartedGoal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class StartedGoalController extends Controller
{
    public function create(Request $request)
    {
        $user = Auth::user();

        if (!$user) {
            abort(403);
        }

        $this->validate($request, [
            'goal_id' => 'required|integer|exists:goals,id',
            'comment' => 'nullable|string|max:255'
        ]);

        if ($user->hasActiveStartedGoal()) {
            abort(403, 'has started goal');
        }

        $startedGoal = $user->startedGoals()->create([
            'goal_id' => $request->input('goal_id'),
            'comment' => $request->input('comment')
        ]);

        $startedGoal->load(['user', 'goal', 'lastGoalPhoto']);

        $startedGoal->setGlobalTop();
        $startedGoal->setGoalTop();
        $startedGoal->setIsLikedBy(Auth::user());

        return new StartedGoalResource($startedGoal);
    }

    public function index(Request $request)
    {
        $this->validate($request, [
            'goal_id' => 'nullable|integer'
        ]);

        $startedGoals = StartedGoal::with('lastGoalPhoto')->whereIsActive(true)->orderBy('score', 'desc');

        if ($request->has('friends')) {
            $user = Auth::user();
            $friendIds = $user->friends()->select('id')->get()->pluck('id');
            $friendIds->push($user->id);
            $startedGoals->whereIn('user_id', $friendIds);
        }

        if ($request->goal_id) {
            $startedGoals->where('goal_id', (int)$request->goal_id);
        }

        $pagination = $startedGoals->paginate(100);

        (new Collection($pagination->items()))->each(function (StartedGoal $startedGoal) {
            $startedGoal->setGlobalTop();
            $startedGoal->setGoalTop();
            $startedGoal->setIsLikedBy(Auth::user());
        });

        return StartedGoalResource::collection($pagination);
    }

    public function show($id)
    {
        /**
         * @var StartedGoal $startedGoal
         */
        $startedGoal = StartedGoal::findOrFail($id);

        $startedGoal->load(['user', 'goal', 'lastGoalPhoto']);

        $startedGoal->setGlobalTop();
        $startedGoal->setGoalTop();
        $startedGoal->setIsLikedBy(Auth::user());

        return new StartedGoalResource($startedGoal);
    }

    public function uploadPhoto($id, Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|image'
        ]);


        $user = Auth::user();

        if (!$user) {
            abort(403);
        }

        /**
         * @var StartedGoal $startedGoal
         */
        $startedGoal = StartedGoal::findOrFail($id);

        if ($user->cant('uploadPhoto', $startedGoal)) {
            abort(403);
        }

        if ($startedGoal->goalPhotos()->whereDate('created_at', Carbon::now())->exists()) {
            throw new ApiException(400, 'Фото на сегодня уже добавлено');
        }

        $goalPhoto = GoalPhoto::createAndUploadMedia($request->file('photo'), $startedGoal->id);

        return new GoalPhotoResource($goalPhoto);
    }

    public function like($id)
    {
        $user = Auth::user();

        if (!$user) {
            abort(403);
        }

        /**
         * @var StartedGoal $startedGoal
         */
        $startedGoal = StartedGoal::findOrFail($id);


        $like = $user->likes()->firstOrCreate([
            'started_goal_id' => $startedGoal->id
        ]);

        return new LikeResource($like);
    }

    public function generateStory($id)
    {
        $user = Auth::user();

        if (!$user) {
            abort(403);
        }

        /**
         * @var StartedGoal $startedGoal
         */
        $startedGoal = StartedGoal::findOrFail($id);

        return new JsonResource([
            'link' => str_replace('public/', '', $startedGoal->generateStory())
        ]);
    }
}
