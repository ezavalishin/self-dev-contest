<?php

namespace App\Http\Controllers\VkUser;

use App\Goal;
use App\Http\Controllers\Controller;
use App\Http\Resources\GoalResource;

class GoalController extends Controller
{
    public function index()
    {
        $goals = Goal::all();

        return GoalResource::collection($goals);
    }
}
