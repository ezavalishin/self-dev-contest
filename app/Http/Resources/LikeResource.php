<?php

namespace App\Http\Resources;

use App\Like;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource
 * @package App\Http\Resources
 * @mixin Like
 */
class LikeResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id
        ];
    }
}
