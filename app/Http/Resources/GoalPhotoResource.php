<?php

namespace App\Http\Resources;

use App\GoalPhoto;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

/**
 * Class UserResource
 * @package App\Http\Resources
 * @mixin GoalPhoto
 */
class GoalPhotoResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'link' => str_replace('public/', '', Storage::disk('public')->url($this->storage_path)),
            'created_at' => (string)$this->created_at
        ];
    }
}
