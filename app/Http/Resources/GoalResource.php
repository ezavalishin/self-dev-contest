<?php

namespace App\Http\Resources;

use App\Goal;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource
 * @package App\Http\Resources
 * @mixin Goal
 */
class GoalResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'emoji' => trim($this->emoji),
            'title' => $this->title
        ];
    }
}
