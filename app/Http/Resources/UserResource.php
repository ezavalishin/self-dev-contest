<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\MissingValue;

/**
 * Class UserResource
 * @package App\Http\Resources
 * @mixin User
 */
class UserResource extends JsonResource
{
    public function toArray($request)
    {
        $authUser = \Auth::user();

        if ($authUser->id === $this->id) {
            $friend = true;
        } else {
            $friend = \Auth::user()->friends->where('id', $this->id)->first();
        }


        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'avatar_200' => $this->avatar_200,
            'messages_are_enabled' => $this->messages_are_enabled,
            'notifications_are_enabled' => $this->notifications_are_enabled,
            'created_at' => (string)$this->created_at,
            'started_goal_id' => $this->getAttribute('started_goal_id') ?? new MissingValue(),
            'days_count' => $this->created_at->diffInDays() + 1,
            'is_friend' => $friend !== null
        ];
    }
}
