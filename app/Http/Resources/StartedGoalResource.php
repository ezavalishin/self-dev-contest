<?php

namespace App\Http\Resources;

use App\StartedGoal;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\MissingValue;

/**
 * Class UserResource
 * @package App\Http\Resources
 * @mixin StartedGoal
 */
class StartedGoalResource extends JsonResource
{
    public function toArray($request)
    {

        $needPhoto = true;

        if ($this->lastGoalPhoto) {
            if ($this->lastGoalPhoto->created_at->isSameDay()) {
                $needPhoto = false;
            }
        }

        return [
            'id' => $this->id,
            'score' => $this->score,
            'likes_count' => $this->likes_count,
            'photos_count' => $this->photos_count,
            'comment' => $this->comment,
            'global_top' => $this->getAttribute('global_top') ?? new MissingValue(),
            'goal_top' => $this->getAttribute('goal_top') ?? new MissingValue(),
            'is_liked' => $this->getAttribute('is_liked') ?? new MissingValue(),
            'need_photo' => $needPhoto,
            'user' => new UserResource($this->whenLoaded('user')),
            'goal' => new GoalResource($this->whenLoaded('goal')),
            'photo' => new GoalPhotoResource($this->whenLoaded('lastGoalPhoto'))
        ];
    }
}
