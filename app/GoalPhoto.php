<?php

namespace App;

use App\Events\GoalPhotoCreated;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Ramsey\Uuid\Uuid;


/**
 * App\GoalPhoto
 *
 * @property int $id
 * @property int $started_goal_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|GoalPhoto newModelQuery()
 * @method static Builder|GoalPhoto newQuery()
 * @method static Builder|GoalPhoto query()
 * @method static Builder|GoalPhoto whereCreatedAt($value)
 * @method static Builder|GoalPhoto whereId($value)
 * @method static Builder|GoalPhoto whereStartedGoalId($value)
 * @method static Builder|GoalPhoto whereUpdatedAt($value)
 * @mixin Eloquent
 * @property string $storage_path
 * @method static Builder|GoalPhoto whereStoragePath($value)
 * @property-read StartedGoal $startedGoal
 */
class GoalPhoto extends Model
{
    protected $dispatchesEvents = [
        'created' => GoalPhotoCreated::class
    ];

    public function startedGoal(): BelongsTo
    {
        return $this->belongsTo(StartedGoal::class);
    }

    public static function createAndUploadMedia(UploadedFile $file, $startedGoalId): GoalPhoto
    {
        $goalPhoto = new self();
        $goalPhoto->started_goal_id = $startedGoalId;

        $image = Image::make($file);
        $image->orientate()->widen(1000);

        $storageFileName = 'public/' . Uuid::uuid6() . '.jpg';

        Storage::put($storageFileName, $image->stream('jpg'));

        $goalPhoto->storage_path = $storageFileName;
        $goalPhoto->save();

        return $goalPhoto;
    }
}
