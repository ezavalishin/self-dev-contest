<?php

namespace App\Listeners;

use App\Events\Contracts\HasStartedGoalContract;

class UpdateStartedGoalScore
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param HasStartedGoalContract $event
     * @return void
     */
    public function handle(HasStartedGoalContract $event)
    {
        $startedGoal = $event->startedGoal();

        $startedGoal->calcScore();
    }
}
