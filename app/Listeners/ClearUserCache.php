<?php

namespace App\Listeners;

use App\Events\Contracts\HasUserContract;
use App\Events\UserUpdated;
use App\User;

class ClearUserCache
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param HasUserContract $event
     * @return void
     * @throws \Exception
     */
    public function handle(HasUserContract $event)
    {
        $user = $event->user();

        if ($event instanceof UserUpdated) {
            $keysToChange = array_keys($user->getChanges());

            if (!empty(array_diff($keysToChange, User::$attributesNotToClearCache))) {
                $user->clearCache();
            }
        } else {
            $user->clearCache();
        }
    }
}
