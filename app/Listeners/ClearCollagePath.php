<?php

namespace App\Listeners;

use App\Events\Contracts\HasStartedGoalContract;
use Illuminate\Support\Facades\Storage;

class ClearCollagePath
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param HasStartedGoalContract $event
     * @return void
     */
    public function handle(HasStartedGoalContract $event)
    {
        $startedGoal = $event->startedGoal();

        if (!$startedGoal->collage_storage_path) {
            return;
        }

        try {
            Storage::delete($startedGoal->collage_storage_path);
        } catch (\Exception $e) {
        }

        $startedGoal->collage_storage_path = null;
        $startedGoal->save();
    }
}
