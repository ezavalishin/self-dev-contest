<?php

namespace App\Policies;

use App\StartedGoal;
use App\User;

class StartedGoalPolicy
{
    public function uploadPhoto(User $user, StartedGoal $startedGoal): bool
    {
        return $startedGoal->user_id === $user->id;
    }
}
