<?php

namespace App\Events\Contracts;

use App\StartedGoal;

interface HasStartedGoalContract
{
    public function startedGoal(): StartedGoal;
}
