<?php

namespace App\Events\Contracts;

use App\User;

interface HasUserContract
{
    public function user(): User;
}
