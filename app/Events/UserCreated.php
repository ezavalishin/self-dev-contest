<?php

namespace App\Events;

use App\Events\Contracts\HasUserContract;
use App\User;

class UserCreated extends Event implements HasUserContract
{
    public $user;

    /**
     * Create a new event instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function user(): User
    {
        return $this->user;
    }
}
