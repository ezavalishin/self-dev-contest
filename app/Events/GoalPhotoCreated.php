<?php

namespace App\Events;

use App\Events\Contracts\HasStartedGoalContract;
use App\GoalPhoto;
use App\StartedGoal;

class GoalPhotoCreated extends Event implements HasStartedGoalContract
{

    public $goalPhoto;

    /**
     * Create a new event instance.
     *
     * @param GoalPhoto $goalPhoto
     */
    public function __construct(GoalPhoto $goalPhoto)
    {
        $this->goalPhoto = $goalPhoto;
    }

    public function startedGoal(): StartedGoal
    {
        return $this->goalPhoto->startedGoal;
    }
}
