<?php

namespace App\Events;

use App\Events\Contracts\HasStartedGoalContract;
use App\Like;
use App\StartedGoal;

class LikeCreated extends Event implements HasStartedGoalContract
{
    public $like;

    /**
     * Create a new event instance.
     *
     * @param Like $like
     */
    public function __construct(Like $like)
    {
        $this->like = $like;
    }

    public function startedGoal(): StartedGoal
    {
        return $this->like->startedGoal;
    }
}
