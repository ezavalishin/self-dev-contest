<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;


/**
 * App\Goal
 *
 * @property int $id
 * @property string $emoji
 * @property string $title
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Goal newModelQuery()
 * @method static Builder|Goal newQuery()
 * @method static Builder|Goal query()
 * @method static Builder|Goal whereCreatedAt($value)
 * @method static Builder|Goal whereEmoji($value)
 * @method static Builder|Goal whereId($value)
 * @method static Builder|Goal whereTitle($value)
 * @method static Builder|Goal whereUpdatedAt($value)
 * @mixin Eloquent
 * @property string|null $slug
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Goal whereSlug($value)
 */
class Goal extends Model
{
    protected $fillable = [
        'emoji',
        'title',
        'slug'
    ];
}
