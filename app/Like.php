<?php

namespace App;

use App\Events\LikeCreated;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;


/**
 * App\Like
 *
 * @property int $id
 * @property int $user_id
 * @property int $started_goal_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Like newModelQuery()
 * @method static Builder|Like newQuery()
 * @method static Builder|Like query()
 * @method static Builder|Like whereCreatedAt($value)
 * @method static Builder|Like whereId($value)
 * @method static Builder|Like whereStartedGoalId($value)
 * @method static Builder|Like whereUpdatedAt($value)
 * @method static Builder|Like whereUserId($value)
 * @mixin Eloquent
 * @property-read StartedGoal $startedGoal
 */
class Like extends Model
{
    protected $fillable = [
        'started_goal_id',
    ];

    protected $dispatchesEvents = [
        'created' => LikeCreated::class
    ];

    public function startedGoal(): BelongsTo
    {
        return $this->belongsTo(StartedGoal::class);
    }
}
