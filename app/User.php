<?php

namespace App;

use App\Events\UserCreated;
use App\Events\UserUpdated;
use App\Services\VkClient;
use Eloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Auth\Authorizable;

/**
 * App\User
 *
 * @property int $id
 * @property bool $notifications_are_enabled
 * @property bool $messages_are_enabled
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $avatar_200
 * @property string|null $bdate
 * @property int $sex
 * @property int|null $utc_offset
 * @property Carbon|null $visited_at
 * @property bool $is_admin
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @method static Builder|User whereAvatar200($value)
 * @method static Builder|User whereBdate($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereFirstName($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereIsAdmin($value)
 * @method static Builder|User whereLastName($value)
 * @method static Builder|User whereMessagesAreEnabled($value)
 * @method static Builder|User whereNotificationsAreEnabled($value)
 * @method static Builder|User whereSex($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @method static Builder|User whereUtcOffset($value)
 * @method static Builder|User whereVisitedAt($value)
 * @mixin Eloquent
 * @property-read Collection|StartedGoal[] $startedGoals
 * @property-read int|null $started_goals_count
 * @property-read Collection|Like[] $likes
 * @property-read int|null $likes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $friends
 * @property-read int|null $friends_count
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    public const CACHE_PREFIX_KEY = 'user_';

    public $incrementing = false;

    public static $attributesNotToClearCache = [
        'visited_at',
        'updated_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'first_name',
        'last_name',
        'avatar_200',
        'bdate',
        'sex',
        'utc_offset',
        'visited_at',
        'notifications_are_enabled',
        'messages_are_enabled'
    ];

    protected $casts = [
        'id' => 'integer',
        'sex' => 'integer',
        'utc_offset' => 'integer',
        'notifications_are_enabled' => 'boolean',
        'messages_are_enabled' => 'boolean',
    ];

    protected $dates = [
        'visited_at'
    ];

    protected $dispatchesEvents = [
        'created' => UserCreated::class,
        'updated' => UserUpdated::class
    ];

    public static function byId($id)
    {
        return self::firstOrCreate([
            'id' => $id
        ]);
    }

    public static function getFromCache($id)
    {
        return Cache::remember(self::CACHE_PREFIX_KEY . $id, Carbon::now()->addMinutes(15), function () use ($id) {
            return User::byId($id);
        });
    }

    public function clearCache()
    {
        Cache::forget(self::CACHE_PREFIX_KEY . $this->id);
    }

    public function startedGoals()
    {
        return $this->hasMany(StartedGoal::class);
    }

    public function hasActiveStartedGoal()
    {
        return $this->startedGoals()->where('is_active', true)->exists();
    }

    public function getActiveStartedGoal()
    {
        return $this->startedGoals()->where('is_active', true)->first();
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    public function friends()
    {
        return $this->belongsToMany(self::class, 'friends', 'user_id', 'friend_id');
    }

    public function updateFriends()
    {
        $friends = (new VkClient())->getFriends($this->id, [
            'id'
        ]);

        $friendIds = (new \Illuminate\Support\Collection($friends))->pluck('id');

        $userExistIds = self::whereIn('id', $friendIds)->select('id')->pluck('id');

        $oldFriendIds = $this->friends()->select('friend_id')->get()->pluck('friend_id');
        $this->friends()->sync($userExistIds);
        $newFriendIds = $this->friends()->select('friend_id')->get()->pluck('friend_id');

        $diffIds = $newFriendIds->diff($oldFriendIds);

        $diffIds->each(function ($id) {
            try {
                DB::table('friends')->insert([
                    'user_id' => $id,
                    'friend_id' => $this->id
                ]);
            } catch (\Exception $e) {

            }
        });
    }
}
