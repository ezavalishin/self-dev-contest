<?php

namespace App\Jobs;

use App\StartedGoal;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class DisableExpiredStartedGoals extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        StartedGoal::whereIsActive(true)
            ->whereDoesntHave('lastGoalPhoto', function (Builder $q) {
                $q->where('created_at', '>', Carbon::now()->subHours(24));
            })->update([
                'is_active' => false
            ]);
    }
}
