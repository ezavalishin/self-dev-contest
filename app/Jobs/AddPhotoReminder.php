<?php

namespace App\Jobs;

use App\Services\VkClient;
use App\StartedGoal;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;

class AddPhotoReminder extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        StartedGoal::whereIsActive(true)
//            ->whereHas('user', function (Builder $q) {
//                $q->where('notifications_are_enabled', true);
//            })
            ->whereDoesntHave('lastGoalPhoto', function (Builder $q) {
                $q->whereDate('created_at', Carbon::now());
            })->chunk(100, function (Collection $collection) {
                $userIds = $collection->pluck('user_id')->toArray();

                (new VkClient())->sendPushes($userIds, 'Добавть фото, чтобы приблизиться к своей цели!');
            });
    }
}
