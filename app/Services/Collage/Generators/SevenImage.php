<?php

namespace App\Services\Collage\Generators;

use Intervention\Image\Facades\Image;
use Tzsk\Collage\Contracts\CollageGenerator;
use Tzsk\Collage\Exceptions\ImageCountException;

class SevenImage extends CollageGenerator
{
    use Helper;

    /**
     * @var \Intervention\Image\Image
     */
    protected $canvas;

    /**
     * @inheritDoc
     * @throws ImageCountException
     */
    public function create($closure = null)
    {
        $this->check(7);

        $this->canvas = Image::canvas($this->file->getWidth(), $this->file->getHeight(), $this->bgColor());

        $this->drawFirst();
        $this->drawSecond();
        $this->drawThird();
        $this->drawFourth();
        $this->drawFive();
        $this->drawSix();
        $this->drawSeven();

        $days = $closure();

        $textBlock = $this->generateTextBlock($days);

        $this->canvas->insert($textBlock,'center', 0, 0);

        return $this->canvas;
    }


    private function drawFirst(): void
    {
        $first = Image::make($this->file->getFiles()[0]);
        $first->fit($this->getReal(170), $this->getReal(200));
        $first->rotate(-10);

        $this->canvas->insert($first, 'top-left', $this->getReal(-54), $this->getReal(-65));
    }

    private function drawSecond(): void
    {
        $first = Image::make($this->file->getFiles()[1]);
        $first->fit($this->getReal(250), $this->getReal(130));
        $first->rotate(-10);

        $this->canvas->insert($first, 'top-left', $this->getReal(134), $this->getReal(-56));
    }

    private function drawThird(): void
    {
        $first = Image::make($this->file->getFiles()[2]);
        $first->fit($this->getReal(200), $this->getReal(150));
        $first->rotate(-10);

        $this->canvas->insert($first, 'top-left', $this->getReal(107), $this->getReal(77));
    }

    private function drawFourth(): void
    {
        $first = Image::make($this->file->getFiles()[3]);
        $first->fit($this->getReal(120), $this->getReal(350));
        $first->rotate(-10);

        $this->canvas->insert($first, 'top-left', $this->getReal(275), $this->getReal(113));
    }

    private function drawFive(): void
    {
        $first = Image::make($this->file->getFiles()[4]);
        $first->fit($this->getReal(150), $this->getReal(220));
        $first->rotate(-10);

        $this->canvas->insert($first, 'top-left', $this->getReal(236), $this->getReal(467));
    }

    private function drawSix(): void
    {
        $first = Image::make($this->file->getFiles()[5]);
        $first->fit($this->getReal(300), $this->getReal(260));
        $first->rotate(-10);

        $this->canvas->insert($first, 'top-left', $this->getReal(-70), $this->getReal(401));
    }

    private function drawSeven(): void
    {
        $first = Image::make($this->file->getFiles()[6]);
        $first->fit($this->getReal(150), $this->getReal(250));
        $first->rotate(-10);

        $this->canvas->insert($first, 'top-left', $this->getReal(-79), $this->getReal(141));
    }
}
