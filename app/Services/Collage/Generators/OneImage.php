<?php

namespace App\Services\Collage\Generators;

use Intervention\Image\Facades\Image;
use Tzsk\Collage\Contracts\CollageGenerator;
use Tzsk\Collage\Exceptions\ImageCountException;

class OneImage extends CollageGenerator
{
    use Helper;

    /**
     * @var \Intervention\Image\Image
     */
    protected $canvas;

    /**
     * @inheritDoc
     * @throws ImageCountException
     */
    public function create($closure = null)
    {
        $this->check(2);

        $this->canvas = Image::canvas($this->file->getWidth(), $this->file->getHeight(), $this->bgColor());

        $this->drawFirst();

        $data = $closure();


        $textBlock = $this->generateTextBlock($days);

        $rect = Image::canvas($this->getReal(150), $this->getReal(150), $this->bgColor());
        $rect->rotate(45);

        $this->canvas->insert($rect, 'center', 0, 0);
        $this->canvas->insert($textBlock,'center', 0, 0);

        return $this->canvas;
    }


    private function drawFirst(): void
    {
        $first = Image::make($this->file->getFiles()[0]);
        $first->fit($this->getReal(375), $this->getReal(331));

        $this->canvas->insert($first, 'top-left', 0, 0);
    }
}
