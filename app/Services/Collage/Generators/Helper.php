<?php

namespace App\Services\Collage\Generators;

use Illuminate\Support\Facades\Lang;
use Intervention\Image\AbstractFont;
use Intervention\Image\Facades\Image;

trait Helper
{
    protected function regularFontPath(): string
    {
        return storage_path('app/fonts/Roboto-Regular.ttf');
    }

    protected function boldFontPath(): string
    {
        return storage_path('app/fonts/Roboto-Bold.ttf');
    }

    private function titleFontSize()
    {
        return $this->getReal(30);
    }

    private function titleLineHeight()
    {
        return $this->getReal(36);
    }

    private function textFontSize()
    {
        return $this->getReal(12);
    }

    private function textLineHeight()
    {
        return $this->getReal(16);
    }

    protected function bgColor(): string
    {
        return '#121212';
    }

    protected function getReal($num)
    {
        return round($num * 2.88);
    }

    private function generateTitle($days): string
    {
        return $days . ' ' . Lang::choice('день|дня|дней', $days);
    }

    private function generateText(): string
    {
        return 'Придерживаюсь' . PHP_EOL . 'своей цели';
    }

    protected function generateTextBlock($days): \Intervention\Image\Image
    {
        $width = 150;

        $title = Image::canvas($this->getReal($width), $this->titleLineHeight())->text($this->generateTitle($days), $this->getReal($width / 2), 0, function (AbstractFont $font) {
            $font->file($this->boldFontPath());
            $font->size($this->titleFontSize());
            $font->color('#fff');
            $font->align('center');
            $font->valign('top');
        });

        $text = Image::canvas($this->getReal($width), $this->textLineHeight() * 2)
            ->text('Придерживаюсь', $this->getReal($width / 2), 0, function (AbstractFont $font) {
                $font->file($this->regularFontPath());
                $font->size($this->textFontSize());
                $font->color('#fff');
                $font->align('center');
                $font->valign('top');
            })->text('своей цели', $this->getReal($width / 2), $this->textLineHeight(), function (AbstractFont $font) {
                $font->file($this->regularFontPath());
                $font->size($this->textFontSize());
                $font->color('#fff');
                $font->align('center');
                $font->valign('top');
            });

        $templateHeight = $title->getHeight() + $text->getHeight();

        $template = Image::canvas($this->getReal($width), $templateHeight);

        $template->insert($title);
        $template->insert($text, 'top-left', 0, $title->getHeight());

        return $template;
    }
}
