<?php

namespace App\Services\Collage\Generators;

use Intervention\Image\Facades\Image;
use Tzsk\Collage\Contracts\CollageGenerator;
use Tzsk\Collage\Exceptions\ImageCountException;

class FourteenImage extends CollageGenerator
{
    use Helper;

    /**
     * @var \Intervention\Image\Image
     */
    protected $canvas;

    /**
     * @inheritDoc
     * @throws ImageCountException
     */
    public function create($closure = null)
    {
        $this->check(14);

        $this->canvas = Image::canvas($this->file->getWidth(), $this->file->getHeight(), $this->bgColor());

        $this->drawFirst();
        $this->drawSecond();
        $this->drawThird();
        $this->drawFourth();
        $this->drawFive();
        $this->drawSix();
        $this->drawSeven();
        $this->drawEight();
        $this->drawNine();
        $this->drawTen();
        $this->drawEleven();
        $this->drawTwelve();
        $this->drawThirteen();
        $this->drawFourteen();

        $days = $closure();

        $textBlock = $this->generateTextBlock($days);

        $this->canvas->insert($textBlock,'top-left', $this->getReal(138), $this->getReal(200));

        return $this->canvas;
    }


    private function drawFirst(): void
    {
        $first = Image::make($this->file->getFiles()[0]);
        $first->fit($this->getReal(175), $this->getReal(175));
        $first->rotate(-15);

        $this->canvas->insert($first, 'top-left', $this->getReal(-22), $this->getReal(-93));
    }

    private function drawSecond(): void
    {
        $first = Image::make($this->file->getFiles()[1]);
        $first->fit($this->getReal(175), $this->getReal(175));
        $first->rotate(-15);

        $this->canvas->insert($first, 'top-left', $this->getReal(152), $this->getReal(-46));
    }

    private function drawThird(): void
    {
        $first = Image::make($this->file->getFiles()[2]);
        $first->fit($this->getReal(60), $this->getReal(175));
        $first->rotate(-15);

        $this->canvas->insert($first, 'top-left', $this->getReal(326), $this->getReal(0));
    }

    private function drawFourth(): void
    {
        $first = Image::make($this->file->getFiles()[3]);
        $first->fit($this->getReal(85), $this->getReal(85));
        $first->rotate(-15);

        $this->canvas->insert($first, 'top-left', $this->getReal(-45), $this->getReal(81));
    }

    private function drawFive(): void
    {
        $first = Image::make($this->file->getFiles()[4]);
        $first->fit($this->getReal(85), $this->getReal(85));
        $first->rotate(-15);

        $this->canvas->insert($first, 'top-left', $this->getReal(42), $this->getReal(104));
    }

    private function drawSix(): void
    {
        $first = Image::make($this->file->getFiles()[5]);
        $first->fit($this->getReal(175), $this->getReal(85));
        $first->rotate(-15);

        $this->canvas->insert($first, 'top-left', $this->getReal(-68), $this->getReal(168));
    }

    private function drawSeven(): void
    {
        $first = Image::make($this->file->getFiles()[6]);
        $first->fit($this->getReal(100), $this->getReal(175));
        $first->rotate(-15);

        $this->canvas->insert($first, 'top-left', $this->getReal(280), $this->getReal(174));
    }

    private function drawEight(): void
    {
        $first = Image::make($this->file->getFiles()[7]);
        $first->fit($this->getReal(175), $this->getReal(175));
        $first->rotate(-15);

        $this->canvas->insert($first, 'top-left', $this->getReal(-115), $this->getReal(255));
    }

    private function drawNine(): void
    {
        $first = Image::make($this->file->getFiles()[8]);
        $first->fit($this->getReal(175), $this->getReal(175));
        $first->rotate(-15);

        $this->canvas->insert($first, 'top-left', $this->getReal(59), $this->getReal(301));
    }

    private function drawTen(): void
    {
        $first = Image::make($this->file->getFiles()[9]);
        $first->fit($this->getReal(175), $this->getReal(85));
        $first->rotate(-15);

        $this->canvas->insert($first, 'top-left', $this->getReal(256), $this->getReal(348));
    }

    private function drawEleven(): void
    {
        $first = Image::make($this->file->getFiles()[10]);
        $first->fit($this->getReal(175), $this->getReal(85));
        $first->rotate(-15);

        $this->canvas->insert($first, 'top-left', $this->getReal(233), $this->getReal(435));
    }

    private function drawTwelve(): void
    {
        $first = Image::make($this->file->getFiles()[11]);
        $first->fit($this->getReal(265), $this->getReal(215));
        $first->rotate(-15);

        $this->canvas->insert($first, 'top-left', $this->getReal(-172), $this->getReal(429));
    }

    private function drawThirteen(): void
    {
        $first = Image::make($this->file->getFiles()[12]);
        $first->fit($this->getReal(85), $this->getReal(175));
        $first->rotate(-15);

        $this->canvas->insert($first, 'top-left', $this->getReal(99), $this->getReal(498));
    }

    private function drawFourteen(): void
    {
        $first = Image::make($this->file->getFiles()[13]);
        $first->fit($this->getReal(175), $this->getReal(175));
        $first->rotate(-15);

        $this->canvas->insert($first, 'top-left', $this->getReal(186), $this->getReal(522));
    }
}
