<?php

namespace App\Services\Collage\Generators;

use Intervention\Image\Facades\Image;
use Tzsk\Collage\Contracts\CollageGenerator;
use Tzsk\Collage\Exceptions\ImageCountException;

class ThreeImage extends CollageGenerator
{
    use Helper;

    /**
     * @var \Intervention\Image\Image
     */
    protected $canvas;

    /**
     * @inheritDoc
     * @throws ImageCountException
     */
    public function create($closure = null)
    {
        $this->check(3);

        $this->canvas = Image::canvas($this->file->getWidth(), $this->file->getHeight(), $this->bgColor());

        $this->drawFirst();
        $this->drawSecond();
        $this->drawThird();

        $days = $closure();

        $textBlock = $this->generateTextBlock($days);

        $this->canvas->insert($textBlock,'top-left', $this->getReal(40), $this->getReal(464));

        return $this->canvas;
    }


    private function drawFirst(): void
    {
        $first = Image::make($this->file->getFiles()[0]);
        $first->fit($this->getReal(300), $this->getReal(400));
        $first->rotate(15);

        $this->canvas->insert($first, 'top-left', $this->getReal(-200), $this->getReal(-22));
    }

    private function drawSecond(): void
    {
        $first = Image::make($this->file->getFiles()[1]);
        $first->fit($this->getReal(300), $this->getReal(400));
        $first->rotate(15);

        $this->canvas->insert($first, 'top-left', $this->getReal(60), $this->getReal(-230));
    }

    private function drawThird(): void
    {
        $first = Image::make($this->file->getFiles()[2]);
        $first->fit($this->getReal(250), $this->getReal(500));
        $first->rotate(15);

        $this->canvas->insert($first, 'top-left', $this->getReal(164), $this->getReal(172));
    }
}
