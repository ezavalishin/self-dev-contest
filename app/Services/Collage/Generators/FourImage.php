<?php

namespace App\Services\Collage\Generators;

use Intervention\Image\Facades\Image;
use Tzsk\Collage\Contracts\CollageGenerator;
use Tzsk\Collage\Exceptions\ImageCountException;

class FourImage extends CollageGenerator
{
    use Helper;

    /**
     * @var \Intervention\Image\Image
     */
    protected $canvas;

    /**
     * @inheritDoc
     * @throws ImageCountException
     */
    public function create($closure = null)
    {
        $this->check(4);

        $this->canvas = Image::canvas($this->file->getWidth(), $this->file->getHeight(), $this->bgColor());

        $this->drawFirst();
        $this->drawSecond();
        $this->drawThird();
        $this->drawFourth();

        $days = $closure();

        $textBlock = $this->generateTextBlock($days);

        $this->canvas->insert($textBlock,'center', 0, 0);

        return $this->canvas;
    }


    private function drawFirst(): void
    {
        $first = Image::make($this->file->getFiles()[0]);
        $first->fit($this->getReal(400), $this->getReal(300));
        $first->rotate(30);

        $this->canvas->insert($first, 'top-left', $this->getReal(-64), $this->getReal(-157));
    }

    private function drawSecond(): void
    {
        $first = Image::make($this->file->getFiles()[1]);
        $first->fit($this->getReal(200), $this->getReal(400));
        $first->rotate(30);

        $this->canvas->insert($first, 'top-left', $this->getReal(218), $this->getReal(132));
    }

    private function drawThird(): void
    {
        $first = Image::make($this->file->getFiles()[2]);
        $first->fit($this->getReal(400), $this->getReal(300));
        $first->rotate(30);

        $this->canvas->insert($first, 'top-left', $this->getReal(-57), $this->getReal(364));
    }

    private function drawFourth(): void
    {
        $first = Image::make($this->file->getFiles()[3]);
        $first->fit($this->getReal(200), $this->getReal(400));
        $first->rotate(30);

        $this->canvas->insert($first, 'top-left', $this->getReal(-217), $this->getReal(89));
    }
}
