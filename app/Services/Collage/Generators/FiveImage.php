<?php

namespace App\Services\Collage\Generators;

use Tzsk\Collage\Contracts\CollageGenerator;
use Tzsk\Collage\Exceptions\ImageCountException;

class FiveImage extends CollageGenerator
{

    /**
     * @inheritDoc
     * @throws ImageCountException
     */
    public function create($closure = null)
    {
        $this->check(5);
    }
}
