<?php

namespace App\Services;

use VK\Client\VKApiClient;

class VkClient
{
    protected $client;
    private $accessToken;

    public const API_VERSION = '5.103';
    public const LANG = 'ru';

    public function __construct($accessToken = null)
    {
        $this->client = new VKApiClient(self::API_VERSION, self::LANG);
        $this->accessToken = $accessToken ?? config('services.vk.app.service_key');
    }

    public function getUsers($ids, array $fields)
    {

        $isFew = is_array($ids);

        $response = $this->client->users()->get($this->accessToken, [
            'user_ids' => $isFew ? $ids : [$ids],
            'fields' => $fields,
        ]);

        return $isFew ? $response : $response[0];
    }

    public function getFriends($userId, $fields): array
    {
        try {
            $response = $this->client->friends()->get($this->accessToken, [
                'user_id' => $userId,
                'fields' => $fields
            ]);
            $users = $response['items'];
        } catch (\Exception $e) {
            $users = [];
        }

        return $users;
    }

    public function sendPushes(array $userIds, string $message): void
    {
        try {
            $result = $this->client->notifications()->sendMessage($this->accessToken, [
                'user_ids' => $userIds,
                'message' => $message
            ]);

            collect($result)->filter(function ($item) {
                return !$item['status'];
            })->filter(function ($item) {
                return $item['error']['code'] === 1;
            })->each(function ($item) {
//                dispatch(new DisableNotificationsForUser($item['user_id']));
            });

        } catch (\Exception $e) {
            return;
        }
    }


//    public function sendMessage(OutgoingMessage $outgoingMessage): void
//    {
//        $this->client->messages()->send($outgoingMessage->getAccessToken(), $outgoingMessage->toVkRequest());
//    }
//
//    public function uploadPhotoInMessage($path, $peerId)
//    {
//        $uploadUrl = $this->client->photos()->getMessagesUploadServer($this->accessToken, [
//            'peer_id' => $peerId
//        ])['upload_url'];
//
//        $client = new Client();
//
//        $response = $client->request('POST', $uploadUrl, [
//            'multipart' => [
//                [
//                    'name' => 'file',
//                    'contents' => fopen(Storage::path($path), 'rb')
//                ],
//            ]
//        ]);
//
//        $file = json_decode($response->getBody(), true);
//
//        Storage::delete($path);
//
//        return $this->client->photos()->saveMessagesPhoto($this->accessToken, [
//            'photo' => $file['photo'],
//            'server' => $file['server'],
//            'hash' => $file['hash'],
//        ]);
//    }
}
