<?php

namespace App\Providers;

use Fruitcake\Cors\CorsServiceProvider;
use Illuminate\Redis\RedisServiceProvider;
use Illuminate\Support\ServiceProvider;
use Intervention\Image\ImageServiceProviderLumen;
use Laravel\Tinker\TinkerServiceProvider;
use Lorisleiva\LaravelDeployer\LaravelDeployerServiceProvider;
use Tzsk\Collage\Provider\CollageServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

        $this->app->register(LaravelDeployerServiceProvider::class);
        $this->app->register(TinkerServiceProvider::class);
        $this->app->register(ImageServiceProviderLumen::class);
        $this->app->register(CorsServiceProvider::class);
        $this->app->register(CollageServiceProvider::class);
        $this->app->register(RedisServiceProvider::class);
    }
}
