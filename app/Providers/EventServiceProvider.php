<?php

namespace App\Providers;

use App\Events\GoalPhotoCreated;
use App\Events\LikeCreated;
use App\Events\UserCreated;
use App\Events\UserUpdated;
use App\Listeners\ClearCollagePath;
use App\Listeners\ClearUserCache;
use App\Listeners\FillUser;
use App\Listeners\UpdateStartedGoalScore;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UserCreated::class => [
            FillUser::class
        ],
        UserUpdated::class => [
            ClearUserCache::class
        ],
        LikeCreated::class => [
            UpdateStartedGoalScore::class
        ],
        GoalPhotoCreated::class => [
            UpdateStartedGoalScore::class,
            ClearCollagePath::class
        ]
    ];
}
