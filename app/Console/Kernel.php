<?php

namespace App\Console;

use App\Commands\StorageLinkCommand;
use App\Jobs\AddPhotoReminder;
use App\Jobs\DisableExpiredStartedGoals;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        StorageLinkCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->job(new AddPhotoReminder())->dailyAt('10:00');
        $schedule->job(new DisableExpiredStartedGoals())->dailyAt('15:00');
    }
}
