<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\AbstractFont;
use Intervention\Image\Image;
use Ramsey\Uuid\Uuid;
use Tzsk\Collage\Facade\Collage;


/**
 * App\StartedGoal
 *
 * @property int $id
 * @property int $user_id
 * @property int $goal_id
 * @property int $score
 * @property int $photos_count
 * @property int $likes_count
 * @property bool $is_active
 * @property string|null $comment
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Goal $goal
 * @property-read User $user
 * @method static Builder|StartedGoal newModelQuery()
 * @method static Builder|StartedGoal newQuery()
 * @method static Builder|StartedGoal query()
 * @method static Builder|StartedGoal whereComment($value)
 * @method static Builder|StartedGoal whereCreatedAt($value)
 * @method static Builder|StartedGoal whereGoalId($value)
 * @method static Builder|StartedGoal whereId($value)
 * @method static Builder|StartedGoal whereIsActive($value)
 * @method static Builder|StartedGoal whereLikesCount($value)
 * @method static Builder|StartedGoal wherePhotosCount($value)
 * @method static Builder|StartedGoal whereScore($value)
 * @method static Builder|StartedGoal whereUpdatedAt($value)
 * @method static Builder|StartedGoal whereUserId($value)
 * @mixin Eloquent
 * @property-read Collection|GoalPhoto[] $goalPhotos
 * @property-read int|null $goal_photos_count
 * @property-read Collection|Like[] $likes
 * @property string|null $collage_storage_path
 * @property-read GoalPhoto $lastGoalPhoto
 * @method static Builder|StartedGoal whereCollageStoragePath($value)
 */
class StartedGoal extends Model
{
    private const MAX_SCORE_CACHE_KEY = 'max_started_goal';

    private const LIKE_RATE = 1;
    private const PHOTO_RATE = 5;

    protected $fillable = [
        'goal_id',
        'score',
        'photos_count',
        'likes_count',
        'is_active',
        'comment'
    ];

    protected $casts = [
        'score' => 'integer'
    ];

    protected $with = [
        'user',
        'goal'
    ];

    protected $attributes = [
        'score' => 0,
        'likes_count' => 0,
        'photos_count' => 0
    ];

    private function photoLines(): array
    {
        return [1, 2, 3, 4, 7, 14];
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function goal(): BelongsTo
    {
        return $this->belongsTo(Goal::class);
    }

    public function goalPhotos(): HasMany
    {
        return $this->hasMany(GoalPhoto::class);
    }

    public function lastGoalPhoto(): HasOne
    {
        return $this->hasOne(GoalPhoto::class)->orderBy('created_at', 'desc');
    }

    public function likes(): HasMany
    {
        return $this->hasMany(Like::class);
    }

    public static function getCachedMax(): self
    {
        return Cache::remember(self::MAX_SCORE_CACHE_KEY, Carbon::now()->addMinutes(15), function () {
            return self::whereIsActive(true)->orderBy('score', 'desc')->first();
        });
    }

    public static function clearCachedMax(): void
    {
        Cache::forget(self::MAX_SCORE_CACHE_KEY);
    }

    public function calcScore(): void
    {
        $this->likes_count = $this->likes()->count();
        $this->photos_count = $this->goalPhotos()->count();
        $this->score = self::LIKE_RATE * $this->likes_count + self::PHOTO_RATE * $this->photos_count;

        $this->save();

//        $maxCached = self::getCachedMax();
//        if (!$maxCached) {
//            return;
//        }
//
//        if ($this->score > $maxCached->score) {
//            self::clearCachedMax();
//        }
    }

    public function setGlobalTop(): void
    {
        $pos = self::where('score', '>', $this->score)->count() + 1;

        $this->setAttribute('global_top', $pos);
    }

    public function setGoalTop(): void
    {
        $pos = self::where('score', '>', $this->score)->where('goal_id', $this->goal_id)->count() + 1;

        $this->setAttribute('goal_top', $pos);
    }

    public function setIsLikedBy(User $user): void
    {
        $isLiked = $user->likes()->where('started_goal_id', $this->id)->exists();

        $this->setAttribute('is_liked', $isLiked);
    }

    public function generateStory()
    {
        if ($this->collage_storage_path) {
            return Storage::disk('public')->url($this->collage_storage_path);
        }

        $lastPhotos = $this->goalPhotos()->orderBy('created_at', 'desc')->limit(14)->get();

        $current = $lastPhotos->count();


        foreach (array_reverse($this->photoLines()) as $count) {
            if ($current >= $count) {
                $current = $count;
                break;
            }
        }

        $lastPhotos = $lastPhotos->take($current);

        $storageImages = $lastPhotos->map(function (GoalPhoto $goalPhoto) {
            return Storage::get($goalPhoto->storage_path);
        });

        /**
         * @var Image $collage
         */

        if ($storageImages->count() < 2) {
            $collage = \Intervention\Image\Facades\Image::canvas(1080, 1920);

            if ($storageImages->isNotEmpty()) {
                $image = \Intervention\Image\Facades\Image::make($storageImages->first());
                $image->fit(1080, 1920);

                $collage->insert($image, 'top-left', 0, 0);
            }

            $overlay = \Intervention\Image\Facades\Image::make(storage_path('app/templates/overlay.png'));
            $collage->insert($overlay, 'top-left', 0, 0);

            $symbol = trim($this->goal->slug);
            $emoji = \Intervention\Image\Facades\Image::make(storage_path("app/templates/{$symbol}.png"))->widen(64 * 3);

            $collage->insert($emoji, 'bottom-center', 0, 164 * 3);


            $commentLines = self::generateText('"' . $this->comment . '"', 1080, 20 * 3, storage_path('app/fonts/Roboto-Regular.ttf'));

            $comment = \Intervention\Image\Facades\Image::canvas(1080 - 100, count($commentLines) * 28 * 3);

            $offset = 0;
            foreach ($commentLines as $line) {
                $comment->text($line, $comment->getWidth() / 2, $offset, function (AbstractFont $font) {
                    $font->size(20 * 3);
                    $font->color('#fff');
                    $font->file(storage_path('app/fonts/Roboto-Regular.ttf'));
                    $font->align('center');
                    $font->valign('top');
                });
                $offset += 28 * 3;
            }

            $collage->insert($comment, 'top-center', 0, 1500);


        } else {
            $collage = Collage::make(1080, 1920)->from($storageImages->toArray(), function () {
                return $this->created_at->diffInDays() + 1;
            });
        }


        $collageFileName = 'public/' . Uuid::uuid6() . '.jpg';

        Storage::put($collageFileName, $collage->stream('jpg'));

        $this->collage_storage_path = $collageFileName;
        $this->save();


        return Storage::disk('public')->url($collageFileName);
    }

    public static function generateText($text, $width, $size, $path): array
    {
        //-- Helpers
        $line = [];
        $lines = [];
        //-- Loop through words
        foreach (explode(' ', $text) AS $word) {
            //-- Add to line
            $line[] = $word;

            //-- Create new text query
            $im = new \Imagick();
            $draw = new \ImagickDraw();
            $draw->setFont($path);
            $draw->setFontSize($size);
            $info = $im->queryFontMetrics($draw, implode(' ', $line) . '...');

            //-- Check within bounds
            if ($info['textWidth'] >= $width) {
                //-- We have gone to far!
                array_pop($line);
                $lines[] = implode(' ', $line);
                //-- Start new line
                unset($line);
                $line[] = $word;
            }
        }

        //-- We are at the end of the string
        $lines[] = implode(' ', $line);
        return $lines;
    }
}
